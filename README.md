# Open-source-software



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!



```
cd existing_repo
git remote add origin https://gitlab.com/IOVOVHS/open-source-software.git
git branch -M main
git push -uf origin main
```


## Name
Open source software remote repository

## License
For open source projects, say how it is licensed.

